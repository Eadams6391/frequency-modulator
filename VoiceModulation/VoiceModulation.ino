/*
   FILE          : VoiveModulation.ino
   PROJECT       : PROG8125
   PROGRAMMER    : Emily Adams
   FIRST VERSION : 2016-04-05
   DESCRIPTION   : Recieves an analog signal on an external pin
       and echoes it on another pin out at twice or half the speed
*/
#include <ADC.h>
#include <ADC_Module.h>
#include <RingBuffer.h>
#include <RingBufferDMA.h>
#include <Bounce.h>

int8_t analogOutPin = A14;
int8_t analogInPin = A0;
//indicator LEDs to show state of modulator, fast/slow are normal red LEDs
//recordOn/recordOff are the red and green pins of an RGB LED
int8_t fastLED = 7;
int8_t slowLED = 8;
int8_t recordOnLED = 4;
int8_t recordOffLED = 6;

Bounce recordPb(10, 10);                  //create a pb object
Bounce fastModePb(11, 10);                   //create a pb object
Bounce slowModePb(12, 10);                       //create a pb object

int8_t checkforPBPress();

ADC *adc = new ADC(); // adc object

 class AudioBuffer
  {
      uint32_t recordingIndex;
      uint32_t buffLen;
      int16_t audioBuffer[500];
      float minAnalogReading;
      float maxAnalogReading;
      float minAnalogWrite;
      float maxAnalogWrite;
      int8_t outDelay_micros;

    public:

      AudioBuffer()
      {
        recordingIndex = 0;
        buffLen = 500;
        audioBuffer[500] = {0};
        minAnalogReading = 0;
        maxAnalogReading = 1023;
        minAnalogWrite = 0;
        maxAnalogWrite = 4095;
        outDelay_micros = 10;
      }
      //
      // FUNCTION      : record
      // DESCRIPTION   :
      //   This function reads an analog voltage from a pin and traces the waveform
      // into a buffer
      // PARAMETERS    :
      //   int8_t analogPin : where to read the voltage
      // RETURNS       :
      //   int8_t : flag to indicate if the buffer is full, 1 if full
      //
      int8_t record(int8_t analogPin)
      {
        int8_t stopRecording = 0;
        if (recordingIndex < buffLen)
        {
          audioBuffer[recordingIndex] = adc->analogRead(analogPin);
          recordingIndex++;
        }
        else
        {
          stopRecording = 1;
        }
        return stopRecording;
      }
       //
      // FUNCTION      : checkIfAtStart
      // DESCRIPTION   :
      //   This function looks at the buffer's current index to see 
      //   if audio has been recorded
      // PARAMETERS    :
      //   none
      // RETURNS       :
      //   int8_t : flag to tell if the recording index == 0
      //
      int8_t checkIfAtStart()
      {
        return recordingIndex == 0;
      }
       //
      // FUNCTION      : formatRecording
      // DESCRIPTION   :
      //   This function takes the analog voltage in the recording buffer
      // and adjusts it to match the range for the DAC output
      // PARAMETERS    :
      //   none
      // RETURNS       :
      //   none
      //
      void formatRecording()
      {
        for (uint32_t x = 0; x < recordingIndex; x++)
        {
          audioBuffer[x] = minAnalogWrite + (audioBuffer[x] - minAnalogReading) * (maxAnalogWrite - minAnalogWrite) / (maxAnalogReading - minAnalogReading);
          //formula sourced from http://mathforum.org/library/drmath/view/60433.html in answer from Dr. Douglas
        }
        return;
      }
       //
      // FUNCTION      : eraseRecording
      // DESCRIPTION   :
      //   This function resets the index of the audio buffer to
      //   zero, allowing the data tobe overwritten
      // PARAMETERS    :
      //   none
      // RETURNS       :
      //   none
      //
      void eraseRecording()
      {
        recordingIndex = 0;
      }

      //
      // FUNCTION      : play
      // DESCRIPTION   :
      //   This function takes the analog voltage trace in the recording buffer
      // and sends it to the DAC at normal speed
      // PARAMETERS    :
      //   int8_t analogPin : where to send the voltage
      // RETURNS       :
      //   none
      //
      void play(int8_t analogPin)
      {
        for (uint16_t x = 0; x < recordingIndex; x++)
        {
          analogWrite(analogPin, audioBuffer[x]);
          delayMicroseconds(outDelay_micros);
        }
        return;
      }
       //
      // FUNCTION      : playFast
      // DESCRIPTION   :
      //   This function takes the analog voltage trace in the recording buffer
      // and sends it to the DAC at 2.5x speed useing and overlap and add algorithm
      // to make the recording playfor the same overall time
      // Algorithm referenced from: http://www.surina.net/article/time-and-pitch-scaling.html
      // PARAMETERS    :
      //   int8_t analogPin : where to send the voltage
      // RETURNS       :
      //   none
      //
      void playFast(int8_t analogPin)
      {
        uint16_t i = 0;
        uint16_t j = 0;
        int16_t avg = 0;
        for (uint16_t x = 0; x < 1000; x++) //2x 500 samples is 1000, so output plays for same length of time as input
        {
          if (x % 190 == 50)//
          {
            i = j;
            analogWrite(analogPin, audioBuffer[i]);//after averaging first 50, switch to new buffer's index
            delayMicroseconds(outDelay_micros/2);
          }
          else if (x % 190 < 50)//every 200 samples, the first 50 are an average of two ends (one starting-j, one finishing-i) of 250 sample size buffers
          {
            avg = (audioBuffer[i] + audioBuffer[j])/2;//for first 50 i=j so averaging does nothing
            analogWrite(analogPin, avg);
            delayMicroseconds(outDelay_micros/2);
            j++;
          }
          else
          {
            analogWrite(analogPin, audioBuffer[i]);//no averaging needed for next 150
            delayMicroseconds(outDelay_micros/2);
          }
          i++;
        }
        return;
      }
       //
      // FUNCTION      : playSlow
      // DESCRIPTION   :
      //   This function takes the analog voltage trace in the recording buffer
      // and sends it to the DAC at half speed
      // PARAMETERS    :
      //   int8_t analogPin : where to send the voltage
      // RETURNS       :
      //   none
      //
      void playSlow(int8_t analogPin)
      {
        int i = 160;
        int j = 320;
        for (uint32_t x = 0; x < 250; x++) //500/2 is 250
        {
          int total = 0;
          int divisor = 0;
          if(x < 180)
          {
            total = total + audioBuffer[x];
            divisor++;
          }
          if(x > 35 && x < 215)
          {
            total = total + audioBuffer[i];
            i++;
            divisor++;
          }
          if(x > 70)
          {
            total = total + audioBuffer[j];
            j++;
            divisor++;
          }
          analogWrite(analogPin, total/divisor);
          delayMicroseconds(outDelay_micros*2);
        }
        return;
      }
  };

void setup() {
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP);
  pinMode(12, INPUT_PULLUP);
  
  pinMode(analogOutPin, OUTPUT);
  
  pinMode(fastLED, OUTPUT);
  pinMode(slowLED, OUTPUT);
  pinMode(recordOnLED, OUTPUT);
  pinMode(recordOffLED, OUTPUT);

  analogWriteResolution(12);
  adc->setAveraging(1); // set number of averages
  adc->setResolution(10); // set bits of resolution
  adc->setSamplingSpeed(ADC_HIGH_SPEED);
  adc->setConversionSpeed(ADC_HIGH_SPEED_16BITS);
  Serial.begin(9600);
  digitalWrite(fastLED, HIGH); 
  digitalWrite(slowLED, LOW);//default to high-pitch playback on startup
  digitalWrite(recordOffLED, HIGH); 
  digitalWrite(recordOnLED, LOW); //not immediatly recording on startup
}

void loop() {
  enum RecorderState
  {
    WAIT,
    RECORD,
    PLAY
  };
  static RecorderState voiceModState = WAIT;
  /*states:    WAIT:   look for button press to beging recording, playing or switch speeds
               RECORD: read analog input from the mic into a buffer
               PLAY: send analog output from buffer to the DAC
  */
  int8_t pbPressed = 0;
  //update the pb's
  recordPb.update();
  fastModePb.update();
  slowModePb.update();
  
  int8_t endOfBuffer = 0; //flag for if end of audio buffer reached
  
  static int8_t fastOrSlow = 1; //flag for if recording should play at double or half speed, 1 for double
  
  static AudioBuffer audioBuff = AudioBuffer();
  
  switch (voiceModState)
  {
    case WAIT:
      pbPressed = checkforPBPress();
      if (pbPressed == 1)
      {
        audioBuff.eraseRecording(); //start a new recording
        voiceModState = RECORD;//start playing/recording
        digitalWrite(recordOnLED, HIGH); 
        digitalWrite(recordOffLED, LOW); //reflect that state on LED
      }
      else if (pbPressed == 2)
      {
        fastOrSlow = 1;
        digitalWrite(fastLED, HIGH); 
        digitalWrite(slowLED, LOW); //indicate high pitch mode on LEDs
      }
      else if (pbPressed == 3)
      {
        fastOrSlow = 0;
        digitalWrite(fastLED, LOW); 
        digitalWrite(slowLED, HIGH); //indicate low pitch mode on LEDs
      }
      break;
    case RECORD:
      endOfBuffer = audioBuff.record(analogInPin);
      pbPressed = recordPb.risingEdge(); //see if record button is no longer pressed
      if (pbPressed == 1)//if record no longer pressed
      {
        voiceModState = WAIT;//stop playing/recording
        digitalWrite(recordOffLED, HIGH); 
        digitalWrite(recordOnLED, LOW); //reflect that state on LED
      }      
      else if (endOfBuffer == 1)//otherwise look to see if buffer full
      {
        audioBuff.formatRecording();//if it is, play the recording
        voiceModState = PLAY;
      }
      break;
    case PLAY:
      if (fastOrSlow == 1)
      {
        audioBuff.playFast(analogOutPin);
      }
      else
      {
        audioBuff.playSlow(analogOutPin);
      }
      pbPressed = recordPb.risingEdge(); //see if record button is no longer pressed
      if (pbPressed == 1)
      {
        voiceModState = WAIT;//stop playing/recording
        digitalWrite(recordOffLED, HIGH); 
        digitalWrite(recordOnLED, LOW); //reflect that state on LED
      }
      else
      {
        audioBuff.eraseRecording(); //start a new recording
        voiceModState = RECORD;
      }
      break;
  }
 }

  // FUNCTION      : checkforPBPress()
  // DESCRIPTION   : Checks whether any of the four
  //                pushbuttons have been pressed
  // PARAMETERS    : none
  // RETURNS       : int8_t, 1 if record pressed, 2
  //                 if fastMode pressed, or 3 if slowMode
  //                 pressed. 0 returned if none
  //                 have been pressed.
  int8_t checkforPBPress()
  {
    if (recordPb.fallingEdge())
    {
      return 1;
    }
    else if (fastModePb.fallingEdge())
    {
      return 2;
    }
    else if (slowModePb.fallingEdge())
    {
      return 3;
    }
    else
    {
      return 0; //as no pushbutton was pressed.
    }
  }
