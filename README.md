# README #

Frequency modulator on the Teensy 3.1 development board. User can select to shift frequency up to 2x speed or down to half speed.

Version 1.1

### Setup ###

* Requires Arduino and Teensyduino libraries to compile
* Shifting selected by pressing one of two pushbuttons
* Output activated by holding down a third
* Analog signal used as input on pin 14 (A0), output sent on pin A14/ the DAC

### Who do I talk to? ###

* Written by: Emily Adams
* Overlap and add algorithm referenced from "Time and pitch scaling in audio processing", by Olli Parviainen, http://www.surina.net/article/time-and-pitch-scaling.html 